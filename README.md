# EscenarioCasa

Programa que dibuja una casa con una nube que se mueve con mouse y un carro que se mueve con flechas de navegación, usando la librería Opengl en java.

Mi proyecto se encuentra en la carpeta EscenarioCasa que contiene los archivos y carpetas del programa:

- build
- nbproject
- dist
- src/org/dchicaiza
- build.xml
- manifest.mf

La carpeta src/org/dchicaiza contiene los archivos .java del proyecto:

- Carro.java
- Casa.java
- Nube.java
- TerrenoCarretera.java
- EscenarioCasa.java (clase principal)

La imagen de ejecución del programa es la siguiente:

![mi imagen](https://www.dropbox.com/s/rjyikbywr89rs6g/escCasa.PNG?dl=0)


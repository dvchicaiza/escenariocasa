/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import static java.lang.Math.*;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class Carro {
    GL gl;
    
    public Carro(GL gl){
        this.gl=gl;
    }
    
    void dibujaCuadrado(float x, float y, float ancho, float alto, float c1, float c2, float c3){   
            gl.glColor3f(c1,c2,c3);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(x, y);
            gl.glVertex2f(x, y+alto);
            gl.glVertex2f(x+ancho, y+alto);
            gl.glVertex2f(x+ancho, y);
            gl.glEnd();
        }
    
    void dibujaPoligIrregular(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float c1, float c2, float c3){
            gl.glColor3f(c1, c2, c3);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(x1, y1);
            gl.glVertex2f(x2, y2);
            gl.glVertex2f(x3, y3);
            gl.glVertex2f(x4, y4);
            gl.glEnd();
    }
    void dibujaCirculo(float posx, float posy, float radio, float c1, float c2, float c3){
        gl.glColor3f(c1, c2, c3);
        gl.glBegin(GL.GL_POLYGON);
        for (float i = 0; i < 360; i++)//va de [0, 360]grados
        {
            float x,y;
            float theta=(float) (i*PI/180);//conversion de grados a radianes
            //conversion de coordenadas polares a rectangulares
            x=(float)(radio*cos(theta));
            y=(float)(radio*sin(theta));
            gl.glVertex2f(x+posx, y+posy);
        }
        gl.glEnd();
    }

    public void dibujarCarro(){
        //base
        dibujaCuadrado(1.15f, 0.42f, 0.8f, 0.28f, 0.0f, 0.5f, 0.5f);
        dibujaPoligIrregular(1.15f, 0.7f, 1.2f, 0.75f, 1.33f, 0.75f, 1.3f, 0.7f, 0.0f, 0.5f, 0.5f);
        dibujaPoligIrregular(1.79f, 0.7f, 1.765f, 0.75f, 1.88f, 0.75f, 1.95f, 0.7f, 0.0f, 0.5f, 0.5f);
        
        dibujaPoligIrregular(1.3f, 0.7f, 1.4f, 0.9f, 1.7f, 0.9f, 1.79f, 0.7f, 0, 0, 1);//base ventana
        dibujaPoligIrregular(1.325f, 0.71f, 1.41f, 0.88f, 1.5f, 0.88f, 1.5f, 0.71f, 0.1f, 0.1f, 0.1f);//ventana izquierda
        dibujaPoligIrregular(1.515f, 0.71f, 1.515f, 0.88f, 1.68f, 0.88f, 1.76f, 0.71f, 0.1f, 0.1f, 0.1f);//ventana derecha
        
        //rueda izquierda
        dibujaCirculo(1.35f, 0.4f, 0.1f, 0.0f, 0.0f, 1.0f);
        dibujaCirculo(1.35f, 0.4f, 0.07f, 0.0f, 0.0f, 0.0f);
        //rueda derecha
        dibujaCirculo(1.75f, 0.4f, 0.1f, 0.0f, 0.0f, 1.0f);
        dibujaCirculo(1.75f, 0.4f, 0.07f, 0.0f, 0.0f, 0.0f);
    }
       
}

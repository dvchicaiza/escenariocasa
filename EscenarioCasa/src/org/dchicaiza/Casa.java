/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class Casa {
    GL gl;
    
    public Casa(GL gl){
        this.gl=gl;

    }
    
        void dibujaCuadrado(float x, float y, float ancho, float alto, float c1, float c2, float c3){   
            gl.glColor3f(c1,c2,c3);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(x, y);
            gl.glVertex2f(x, y+alto);
            gl.glVertex2f(x+ancho, y+alto);
            gl.glVertex2f(x+ancho, y);
            gl.glEnd();
        }
        void dibujaTecho(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float c1, float c2, float c3){
            gl.glColor3f(c1, c2, c3);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(x1, y1);
            gl.glVertex2f(x2, y2);
            gl.glVertex2f(x3, y3);
            gl.glVertex2f(x4, y4);
            gl.glEnd();
        }
        
        public void dibujarCasa(){
            gl.glPushMatrix();
            dibujaCuadrado(1f, 1.5f, 1.5f, 0.6f, 1f, 0.5f, 0f);//bloque primer piso
            dibujaCuadrado(2.1f, 1.5f, 0.25f, 0.4f, 0.35f, 0.0f, 0.0f);//puerta
            dibujaCuadrado(2.3f, 1.7f, 0.03f, 0.03f, 1f, 0.0f, 0.0f);//manija de la puerta
            dibujaCuadrado(1.15f, 1.74f, 0.8f, 0.25f, 0.3f, 0.0f, 0.0f);//fondo ventana
            dibujaCuadrado(1.17f, 1.77f, 0.16f, 0.2f, 0.0f, 0.5f, 1.0f);//cristal 1
            dibujaCuadrado(1.35f, 1.77f, 0.39f, 0.2f, 0.0f, 0.5f, 1.0f);//cristal medio
            dibujaCuadrado(1.76f, 1.77f, 0.16f, 0.2f, 0.0f, 0.5f, 1.0f);//cristal 2
            dibujaCuadrado(1f, 2.1f, 1.5f, 0.05f, 1.0f, 0.25f, 0.0f);//separacion de los pisos
            dibujaCuadrado(1f, 2.15f, 1.5f, 0.6f, 1f, 0.5f, 0f);//bloque segundo piso
            dibujaCuadrado(1.15f, 2.35f, 0.4f, 0.25f, 0.3f, 0.0f, 0.0f);//fondo ventana izquierda
            dibujaCuadrado(1.17f, 2.38f, 0.16f, 0.2f, 0.0f, 0.5f, 1.0f);//cristal 3
            dibujaCuadrado(1.36f, 2.38f, 0.16f, 0.2f, 0.0f, 0.5f, 1.0f);//cristal 4
            dibujaCuadrado(1.95f, 2.35f, 0.4f, 0.25f, 0.3f, 0.0f, 0.0f);//fondo ventana derecha
            dibujaCuadrado(1.97f, 2.38f, 0.16f, 0.2f, 0.0f, 0.5f, 1.0f);//cristal 5
            dibujaCuadrado(2.16f, 2.38f, 0.16f, 0.2f, 0.0f, 0.5f, 1.0f); //cristal 6
            dibujaTecho(0.98f, 2.75f, 1.15f, 3f, 2.35f, 3f, 2.52f, 2.75f, 1f, 0f, 0f);//techo
            gl.glPopMatrix();        
        }
}


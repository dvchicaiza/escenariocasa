package org.dchicaiza;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.GLAutoDrawable;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

// Librerias para teclas
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
//librerias para mouse
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

// Llamar las funciones para las transformaciones
import com.sun.opengl.util.Animator;

/**
 * EscenarioCasa.java 
 * Programa que realiza una casa con un carro que se desplaza con flechas de navegacon y nube que se mueve con mouse
 * Autor: Doris Chicaiza
 *30/07/2020
 */
public class EscenarioCasa extends JFrame implements KeyListener, MouseMotionListener {
    //variables de Opengl
    static  GL gl;
    static GLU glu;

    // Variables para las transformaciones del carro
    private static  float trasladaX=0f;
    private static  float trasladaY=0f;
    //Variables para fijar un punto para interactuar con el mouse
    static float posx=0,posy=0;
    //variables para trasladar la nube
    static float tx=0,ty=0;
    
    public  EscenarioCasa(){
        setTitle("Escenario Casa");
        setSize(700,700);
        setLocation(50,0);
        
        // Intancia de clase GraphicListener        
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas= new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        
        Animator animator = new Animator(canvas);
        animator.start();
        
        addKeyListener(this); // Para que canvas reconozca las pulsaciones del teclado
        canvas.addMouseMotionListener(this);//Para que canvas reconozca los movimientos del mouse         
    }
    
    public static void main(String args[]){
        EscenarioCasa frame = new EscenarioCasa();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);       
    }

    
    public class GraphicListener implements GLEventListener{

        @Override
        public void init(GLAutoDrawable arg0) {
         GL gl = arg0.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); 
        }
        
        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
            GL gl = drawable.getGL();
            GLU glu = new GLU();

            if (height <= 0) { // avoid a divide by zero error!

                height = 1;
            }
            final float h = (float) width / (float) height;
            gl.glViewport(0, 0, width, height);
            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();
            //define los limites y produndidad de la ventana
            gl.glOrtho(0, 700, 0, 700, -1.0, 1.0);
            gl.glMatrixMode(GL.GL_MODELVIEW);
            gl.glLoadIdentity();
        }

        @Override
        public void display(GLAutoDrawable arg0) {
            glu = new GLU();
            gl = arg0.getGL();
           // Limpia el bufer para no tener rastro del movimiento
            gl.glClear(gl.GL_COLOR_BUFFER_BIT);
            // Reset the current matrix to the "identity"
            gl.glLoadIdentity();

            // Definir el grosor de la linea
            gl.glLineWidth(2f);
            // Definir el grosor de los puntos
            gl.glPointSize(3.0f);

            
            gl.glPushMatrix();
            gl.glScalef(180, 180, 0);
            TerrenoCarretera terreno = new TerrenoCarretera(gl);//crea el terreno y la carretera
            terreno.dibujarTerrenoCarretera();
            gl.glPopMatrix();
            
            
            gl.glPushMatrix();
            gl.glScalef(180, 180, 0);
            Casa casita=new Casa(gl);//crea la casa 
            casita.dibujarCasa();
            gl.glPopMatrix();

            
            gl.glPushMatrix();
            gl.glScalef(180, 180, 0);//escala el carro al tama�o deseado
            gl.glTranslatef(trasladaX, trasladaY, 0);//traslada el carro en el eje x y el eje y
            Carro carrito= new Carro(gl);//crea el carro
            carrito.dibujarCarro();
            gl.glPopMatrix();
            

            gl.glPushMatrix(); 
            gl.glTranslatef(tx, ty, 0);//traslada la nube en el ejex y el ejey
            gl.glScalef(180, 180, 0);//escala la nube al tama�o deseado
                Nube nube=new Nube(gl);//crea el objeto nube de tipo Nube
                nube.dibujarNube();
                
                posx=tx+350;//fija un punto en la nube
                System.out.println("fasd "+posx);
            gl.glPopMatrix();

            // Para que se ejecute correctamente subrutinas*/
            gl.glFlush();      
        }

                

        @Override
        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
            
        }
        
    }
    
    
    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    //controla el movimiento del mouse
    public void mouseMoved(MouseEvent e) {
            while(e.getX()>posx){
                tx+=10f;
                posx+=10f;
            }
            while(e.getX()<posx){
                tx-=10f;
                posx-=10f;
            }
            while(e.getY() > posy){
                ty-=10f;
                posy=posy+10f;
            }
            while(e.getY() < posy){
                ty+=10f;
                posy=posy-10f;
            }
    }
    

     @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    //controla que hacen las teclas de direccion al ser pulsadas
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode()== KeyEvent.VK_RIGHT){
            if(trasladaX<2.01f){
                trasladaX+=0.1;
            }else{
                trasladaX+=0;
            }
               
            System.out.println("Valor en la traslacion de X: "+trasladaX);
        }
        
        if(e.getKeyCode()== KeyEvent.VK_LEFT){
            if(trasladaX>-1.15f){
                trasladaX-=0.1;
            }else{
                trasladaX-=0;
            }
            System.out.println("Valor en la traslacion de X: "+trasladaX);
        }
        
        if(e.getKeyCode()== KeyEvent.VK_UP){
            if(trasladaY<0.25f){
                trasladaY+=0.1;
            }else{
                trasladaY+=0;
            }
            System.out.println("Valor en la traslacion de Y: "+trasladaY);
        }
        
        if(e.getKeyCode()== KeyEvent.VK_DOWN){
            if(trasladaY>-0.25f){
                trasladaY-=0.1;;
            }else{
                trasladaY-=0;
            }
            System.out.println("Valor en la traslacion de Y: "+trasladaY);
        }
       
         if(e.getKeyCode()== KeyEvent.VK_ESCAPE){
            trasladaX=0;
            trasladaY=0;
        }        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class TerrenoCarretera {
    GL gl;
    
    public TerrenoCarretera(GL gl){
        this.gl=gl;
    }
    
    void dibujaCuadrado(float x, float y, float ancho, float alto, float c1, float c2, float c3){   
            gl.glColor3f(c1,c2,c3);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(x, y);
            gl.glVertex2f(x, y+alto);
            gl.glVertex2f(x+ancho, y+alto);
            gl.glVertex2f(x+ancho, y);
            gl.glEnd();
        }
    void dibujaTriangulo(float x, float y, float ancho, float alto, float c1, float c2, float c3){
        gl.glColor3f(c1,c2,c3);
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glVertex2f(x, y);
        gl.glVertex2f(x+ancho, y);
        gl.glVertex2f(x+(ancho/2), y+alto);
        gl.glEnd();
        
    }
    public void dibujarTerrenoCarretera(){
        dibujaCuadrado(0, 0.7f, 4, 0.8f, 0, 1, 0);//area verde
        dibujaTriangulo(3f, 1.8f, 0.4f, 0.5f, 0, 1, 0);//arbol
        dibujaCuadrado(3.15f, 1.5f, 0.1f, 0.3f, 0.5f, 0.0f, 0.0f);//tronco arbol
        //lineas de division de la carretera
        dibujaCuadrado(0.2f, 0.3f, 0.6f, 0.1f, 1, 1, 1);
        dibujaCuadrado(1.2f, 0.3f, 0.6f, 0.1f, 1, 1, 1);
        dibujaCuadrado(2.2f, 0.3f, 0.6f, 0.1f, 1, 1, 1);
        dibujaCuadrado(3.2f, 0.3f, 0.6f, 0.1f, 1, 1, 1);
    }
}

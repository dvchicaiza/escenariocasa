/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import javax.media.opengl.GL;
import static java.lang.Math.*;
/**
 *
 * @author Doris
 */
public class Nube {
    GL gl;
    
    public Nube(GL gl){
        this.gl=gl;
    }


    void dibujaCirculo(float posx, float posy, float radio, float c1, float c2, float c3){
        gl.glColor3f(c1, c2, c3);
        gl.glBegin(GL.GL_POLYGON);
        for (float i = 0; i < 360; i++)//va de [0, 360]grados
        {
            float x,y;
            float theta=(float) (i*PI/180);//conversion de grados a radianes
            //conversion de coordenadas polares a rectangulares
            x=(float)(radio*cos(theta));
            y=(float)(radio*sin(theta));
            gl.glVertex2f(x+posx, y+posy);
        }
        gl.glEnd();
    }
    
    public void dibujarNube(){
//        gl.glPushMatrix();
        dibujaCirculo(1.92f, 3.48f, 0.15f, 1, 1, 1);
        dibujaCirculo(2.15f, 3.52f, 0.15f, 1, 1, 1);
        dibujaCirculo(1.75f, 3.55f, 0.15f, 1, 1, 1);
        dibujaCirculo(2.08f, 3.4f, 0.15f, 1, 1, 1);
//        gl.glPopMatrix();
    }
    
}
